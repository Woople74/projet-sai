#include <GL/gl.h>
#include <GL/glut.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "time.h"
#include "include/intersection.h"
#include "include/matrice.h"
#include "include/generation.h"
#include "include/objets.h"

#define WIDTH 1900
#define HEIGHT 1000

#define degToRad(angleInDegrees) ((angleInDegrees) * M_PI / 180.0)
#define radToDeg(angleInRadians) ((angleInRadians) * 180.0 / M_PI)

/*stairsx d'objets */
praa sol[1000000];
praa cube[1000000];
praa cloisons[100000];
praa cloisonsMaison[10000];
praa arbre[10000];
praa petitCube[10000];
escalier stairs[10000];

/* Matrice R associée rotation pi/3 rapport au point O */
matrice T0;

/* Angle pour vecteur direction et souris*/
float angle = 0;
int sens = 0;

/*Nombre de cubes récupérés*/
int nbCubes = 0;

/* Position du joueur/caméra */
float posX = 0;
float posY = 15;
float posZ = 0;

/* Vecteur direction du joueur */
float vecx = 0;
float vecy = 0;
float vecz = -1;

/* Vecteur vitesse pas implémenté je crois */
float vitesseX = 1.5;
float vitesseY = 1.5;
float vitesseZ = 1.5;

float epsilonVitesse = 0.1;

/* Vecteur qui indique le haut du perso */
float upX = 0;
float upY = 1;
float upZ = 0;

/* Coordonées de la souris */
float lastMouseX = 400, lastMouseY = 300;
int firstMouse = 1;

/* Variable de chute libre */
int chute = 0;
int jump = 0;
float limite_saut = 1.5;
float indice = 0;

/* Variable pour les fins de marches */
int tab_info[10];
int numero_esca = 0;
int numero_cm = 0;
int info = 0;
int col_escalier = 0;

/* Fonction pour savoir si un arbre est en position (x, z)
retourne la position de l'arbre dans le tableau si c'est vrai
-1 sinon */
int estArbre(int x, int z){
  int i;
  for (i = 1; i <=arbre[0].hauteur; i++) {
    if(x == arbre[i].pminX && z == arbre[i].pminZ) return i;
  }  
  return -1;
}

/* Variables Timer */
clock_t start;
int actual = 0; 
int stop = 120000;

/* ######################################################### */
/* ################### FONCTIONS TIMER ##################### */
/* ######################################################### */

void vBitmapOutput(int x, int y, char *string, void *font)
{
	int len,i; // len donne la longueur de la chaîne de caractères
	glRasterPos2f(x,y); // Positionne le premier caractère de la chaîne
	len = (int) strlen(string); // Calcule la longueur de la chaîne
	for (i = 0; i < len; i++) {
    glutBitmapCharacter(font,string[i]); // Affiche chaque caractère de la chaîne
  }
}

void vStrokeOutput(GLfloat x, GLfloat y, char *string, void *font)
{
	char *p;
	glPushMatrix();	
  // glPushMatrix et glPopMatrix sont utilisées pour sauvegarder 
	// et restaurer les systèmes de coordonnées non translatés
	glTranslatef(x, y, 0); // Positionne le premier caractère de la chaîne
	for (p = string; *p; p++) {
    glutStrokeCharacter(font, *p); // Affiche chaque caractère de la chaîne
  }
	glPopMatrix();
}

/* ######################################################### */
/* ################ FONCTIONS TIMER (FIN) ################## */
/* ######################################################### */

/* ######################################################### */
/* ## FONCTIONS POUR AJOUTER LES OBJETS DANS LES TABLEAUX ## */
/* ######################################################### */

/* Fonction qui crée un sol & ajout dans le tableau */
void creer_sol () {
  praa p1;
  p1.hauteur = 0;
  p1.pmaxX = 100;
  p1.pmaxY = 0;
  p1.pmaxZ = 100;
  p1.pminX = -100;
  p1.pminY = 0;
  p1.pminZ = -100;
    
  ajouter_tab (sol, p1);
}

/* Fonction de création d'une cloison (sous forme de praa) & ajout dans le tableau */
void creer_cloison (int x1, int y1, int z1, int x2, int y2, int z2, int hauteur) {
  praa p1;
  p1.hauteur = hauteur;
  p1.pminX = x1;
  p1.pminY = y1;
  p1.pminZ = z1;
  p1.pmaxX = x2;
  p1.pmaxY = y2;
  p1.pmaxZ = z2;

  ajouter_tab(cloisons, p1);
  ajouter_tab(cloisonsMaison, p1);
}

/* Fonction pour créer un premier étage & ajout dans le tableau */
void creer_permier_etage (int hauteur) {
  praa p1;
  p1.hauteur = 0;
  p1.pmaxX = 10;
  p1.pmaxY = hauteur;
  p1.pmaxZ = 40;
  p1.pminX = -10;
  p1.pminY = hauteur;
  p1.pminZ = 10;
    
  ajouter_tab (sol, p1);
}

/* Fonction de création d'un escalier en x,y,z avec sa largeur, hauteur etc & ajout dans le tableau */
void creer_escalier (int x, int y, int z, int largeur, int hauteur, int longueur, char orientation) {
  escalier e;
  e.hauteur = hauteur;
  e.largeur = largeur;
  e.longueur = longueur;
  e.orientation = orientation;
  e.x = x;
  e.y = y;
  e.z = z;

  ajouter_escalier(stairs, e);
  
  int i;
  for (i = 0; i < longueur; i++) {
    praa p1;
    p1.hauteur = hauteur;
    p1.pmaxX = x+largeur;
    p1.pmaxY = y+hauteur;
    p1.pmaxZ = z+hauteur;
    p1.pminX = x;
    p1.pminY = y+hauteur;
    p1.pminZ = z;

    ajouter_tab(sol, p1);

    y += hauteur;
    z += hauteur;
  }
}

/* Fonction de création d'une maison & ajout dans le tableau */
void creer_maison(float x, float y, float z){
  /* Cloisons extérieures */
  creer_cloison(x , y, z, x, y+8, z+8, 6);
  creer_cloison(x , y, z+8, x+8, y+8, z+8, 6);
  creer_cloison(x+8 , y, z, x+8, y+8, z+8, 6);
  /*Face de la porte*/
  creer_cloison(x+6, y, z, x+8, y+4, z, 2);
  creer_cloison(x, y, z, x+2, y+4, z, 2);
  creer_cloison(x, y+4, z, x+8, y+8, z, 2);
  /*coté des escaliers*/
  creer_cloison(x+4,y,z+3, x+4, y+1, z+5, 1);
  creer_cloison(x+4,y, z+4, x+4, y+2, z+6, 2);
  creer_cloison(x+4,y,z+5,x+4,y+3,z+8, 3);
  creer_cloison(x+4,y,z+6,x+4,y+4,z+8, 4);

  creer_cloison(x+2,y,z+3, x+2, y+1, z+5, 1);
  creer_cloison(x+2,y, z+4, x+2, y+2, z+6, 2);
  creer_cloison(x+2,y,z+5,x+2,y+3,z+8, 3);
  creer_cloison(x+2,y,z+6,x+2,y+4,z+8, 4);
  /* Sol du premier etage */
  praa p1;
  p1.hauteur = 0;
  p1.pmaxX = x+8;
  p1.pmaxY = y;
  p1.pmaxZ = z+8;
  p1.pminX = x;
  p1.pminY = y;
  p1.pminZ = z;
    
  ajouter_tab (sol, p1);

  /* Sol du deuxième étage */
  praa p2;
  p2.hauteur = 0;
  p2.pmaxX = x+8;
  p2.pmaxY = y+4;
  p2.pmaxZ = z+8;
  p2.pminX = x+4;
  p2.pminY = y+4;
  p2.pminZ = z;
    
  ajouter_tab (sol, p2);

  /* Petit bout de sol au bout de l'escalier */
  praa p3;
  p3.hauteur = 0;
  p3.pmaxX = x+4;
  p3.pmaxY = y+4;
  p3.pmaxZ = z+8;
  p3.pminX = x+2;
  p3.pminY = y+4;
  p3.pminZ = z+7;

  ajouter_tab (sol, p3);

  /* Deuxieme partie de l'etage */
  praa p5;
  p5.hauteur = 0;
  p5.pmaxX = x+2;
  p5.pmaxY = y+4;
  p5.pmaxZ = z+8;
  p5.pminX = x;
  p5.pminY = y+4;
  p5.pminZ = z;
    
  ajouter_tab (sol, p5);

  /* toit */
  praa p4;
  p4.hauteur = 0;
  p4.pmaxX = x+8;
  p4.pmaxY = y+8;
  p4.pmaxZ = z+8;
  p4.pminX = x;
  p4.pminY = y+8;
  p4.pminZ = z;
    
  ajouter_tab (sol, p4);

  creer_escalier(x+2, y, z+3, 2, 1, 4, 'z');
}

/* Fonction de création d'un arbre & ajout dans le tableau */
void creer_arbre(float x, float y, float z, int hauteur_tronc, int hauteur_arbre) {
  /* troncs */
  praa p1;
  p1.hauteur = hauteur_arbre;
  p1.pmaxX = x+1;
  p1.pmaxY = y+hauteur_tronc;
  p1.pmaxZ = z+1;
  p1.pminX = x;
  p1.pminY = y;
  p1.pminZ = z;

  ajouter_tab(arbre, p1);
  ajouter_tab(cloisons, p1);  
}

void poser_maison(int x, int z){
  int i,j,k,l,a, y = 15, verif;
  praa p1;
  creer_maison(x, y, z);
  for(y=15;y >= 8; y--){
    a = 16 - y; //Permet de savoir a quelle ligne on est
    for(i=x-a; i<=x+6+a; i++){
      
      p1.hauteur = y;
      p1.pmaxX = i+1;
      p1.pmaxY = y; 
      p1.pmaxZ = z-a+1;
      p1.pminX = i;
      p1.pminY = 0;
      p1.pminZ = z-a;

      verif = estArbre(i, z-a);
      if(verif != -1){
        //Retirer l'arbre de la liste d'arbre et cloisons
        enlever_tab(arbre, verif);
      }

      ajouter_tab(sol, p1);
      ajouter_tab(cube, p1);
    }
    
    for(j=z-a; j<=z+6+a; j++){
      
      p1.hauteur = y;
      p1.pmaxX = x+8+a;
      p1.pmaxY = y; 
      p1.pmaxZ = j+1;
      p1.pminX = x+7+a;
      p1.pminY = 0;
      p1.pminZ = j;

      verif = estArbre(x+7+a, j);
      if(verif != -1){
        //Retirer l'arbre de la liste d'arbre et cloisons
        enlever_tab(arbre, verif);
      }

      ajouter_tab(sol, p1);
      ajouter_tab(cube, p1);
    }

    for(k=x-a; k<=x+7+a; k++){
      
      p1.hauteur = y;
      p1.pmaxX = k+1;
      p1.pmaxY = y; 
      p1.pmaxZ = z+8+a;
      p1.pminX = k;
      p1.pminY = 0;
      p1.pminZ = z+7+a;

      verif = estArbre(k, z+7+a);
      if(verif != -1){
        //Retirer l'arbre de la liste d'arbre et cloisons
        enlever_tab(arbre, verif);
      }

      ajouter_tab(sol, p1);
      ajouter_tab(cube, p1);
    }

    for(l=z-a; l<=z+6+a; l++){
      
      p1.hauteur = y;
      p1.pmaxX = x-a+1;
      p1.pmaxY = y; 
      p1.pmaxZ = l+1;
      p1.pminX = x-a;
      p1.pminY = 0;
      p1.pminZ = l;

      verif = estArbre(x-a, l);
      if(verif != -1){
        //Retirer l'arbre de la liste d'arbre et cloisons
        enlever_tab(arbre, verif);
      }

      ajouter_tab(sol, p1);
      ajouter_tab(cube, p1);
    }
  }
  //On fait la verification des arbres pour l'intérieur de la maison
  for(i=x;i<=x+8;i++){
    for(j=z;j<=z+8;j++){
      verif = estArbre(i, j);
      if(verif != -1){
        //Retirer l'arbre de la liste d'arbre et cloisons
        enlever_tab(arbre, verif);
      }
    }
  }
}

//Bien laisser a la fin pour que la fonction aie accès aux autres
/* Fonction de la génération aléatoire du terrain */
void generer_terrain(int largeur, int hauteur){
  double ** hm = create_heightmap(largeur, hauteur);
  int i,j,z,h,x;
  for(i=0; i<largeur; i++){
    for(j=0; j<hauteur; j++){
      praa p1;
      p1.hauteur = floor((hm[i][j] + 1) * 10);
      p1.pmaxX = i+1-(largeur/2);
      p1.pmaxY = floor((hm[i][j] + 1) * 10); 
      p1.pmaxZ = j+1-(hauteur/2);
      p1.pminX = i-(largeur/2);
      p1.pminY = 0;
      p1.pminZ = j-(hauteur/2);

      ajouter_tab(sol, p1);
      ajouter_tab(cube, p1);
    }
    if(i%2 == 0 && i != 0){
      z = (rand() % hauteur);
      h = (rand() % 3) + 3;
      creer_arbre(i- (largeur/2), floor((hm[i][z] + 1) * 10), z - (hauteur/2), h, h*2);
    }else{
      z = (rand() % hauteur);

      praa c1;
      c1.hauteur = 0.33;
      c1.pmaxX = i- (largeur/2) + 0.66;
      c1.pmaxY = floor((hm[i][z] + 1) * 10) + 0.66;
      c1.pmaxZ = z - (hauteur/2) + 0.66;
      c1.pminX = i- (largeur/2) + 0.33;
      c1.pminY = floor((hm[i][z] + 1) * 10) + 0.33;
      c1.pminZ = z - (hauteur/2) + 0.33;

      ajouter_tab(petitCube, c1);
    }
  }
  for(i=0;i<largeur;i+=50){//On va faire spawn une maison par zone de 50 par 50
    x = (rand() % 50) + (i - 50);
    z = (rand() % 50) + (i - 50);
    poser_maison(x, z);
  }

  z = (rand() % hauteur);
}

/* ######################################################### */
/* ## FONCTIONS POUR AJOUTER LES OBJETS DANS LES TABLEAUX ## */
/* ##################### -- (FIN) -- ####################### */
/* ######################################################### */


/* ######################################################### */
/* ########## FONCTIONS POUR AFFICHER LES OBJETS ########### */
/* ######################################################### */

/* Fonction d'affichage d'un pixel */
void trace_pixel (float x, float y, float taille) {
    /*  Déclaration de points  */
    glBegin(GL_QUADS);
    glVertex2f(x-0.5,y+0.5);
    glVertex2f(x+0.5,y+0.5);
    glVertex2f(x+0.5,y-0.5);
    glVertex2f(x-0.5,y-0.5);
    glEnd();
    /*Fin Déclaration de points*/
}

/* Fonction d'affichage d'un sol */
void affiche_sol () {
    glBegin(GL_QUADS);
    glColor3f(1, 0, 1);
    glVertex3f(-100, 0, -100);
    glVertex3f(100, 0, -100);
    glVertex3f(100, 0, 100);
    glVertex3f(-100, 0, 100);
    glEnd();
}

/* Fonction d'affichage d'un étage */
void affiche_premier_etage (int hauteur) {
    glBegin(GL_QUADS);
    glColor3f(0, 0, 1);
    glVertex3f(-10, hauteur, 10);
    glVertex3f(10, hauteur, 10);
    glVertex3f(10, hauteur, 40);
    glVertex3f(-10, hauteur, 40);
    glEnd();
}

/* Fonction d'affichage du contour du perso pour l'intersection */
void affiche_p (float x, float y, float z) {
  glBegin(GL_QUADS);
  /* face avant tronc (orienté vers z) */
  glColor3f(0.25, 0.25, 0.20);
  glVertex3f(x-0.5, y-2, z-0.5);
  glVertex3f(x+0.5, y-2, z-0.5);
  glVertex3f(x+0.5, y+0.5, z-0.5);
  glVertex3f(x-0.5, y+0.5, z-0.5);

  /* face droite tronc (orienté vers z) */
  glColor3f(0.25, 0.25, 0.20);
  glVertex3f(x+0.5, y-2, z-0.5);
  glVertex3f(x+0.5, y-2, z+0.5);
  glVertex3f(x+0.5, y+0.5, z+0.5);
  glVertex3f(x+0.5, y+0.5, z-0.5);

  /* face arrière tronc (orienté vers z) */
  glColor3f(0.25, 0.25, 0.20);
  glVertex3f(x-0.5, y-2, z+0.5);
  glVertex3f(x+0.5, y-2, z+0.5);
  glVertex3f(x+0.5, y+0.5, z+0.5);
  glVertex3f(x-0.5, y+0.5, z+0.5);

  /* face gauche tronc (orienté vers z) */
  glColor3f(0.25, 0.25, 0.20);
  glVertex3f(x-0.5, y-2, z-0.5);
  glVertex3f(x-0.5, y-2, z+0.5);
  glVertex3f(x-0.5, y+0.5, z+0.5);
  glVertex3f(x-0.5, y+0.5, z-0.5);
  glEnd();
}

/* Affichae d'un escalier donné */
void affiche_escalier(int x, int y, int z, int largeur, int hauteur, int longueur) {
    glBegin(GL_QUADS);

    int i = 0;

    for (i = 0; i < longueur; i++) {
      glColor3f(0.9, 0.9, 0.9);
      glVertex3f(x, y, z);
      glVertex3f(x, y+hauteur, z);
      glVertex3f(x+largeur, y+hauteur, z);
      glVertex3f(x+largeur, y, z);

      glColor3f(0.8, 0.8, 0.8);
      glVertex3f(x, y+hauteur, z);
      glVertex3f(x+largeur, y+hauteur, z);
      glVertex3f(x+largeur, y+hauteur, z+hauteur);
      glVertex3f(x, y+hauteur, z+hauteur);

      y += hauteur;
      z += hauteur;
    }
    glEnd();
}

/* Affichage de tous les escaliers */
void affiche_all_stairs () {
  int i, j;
  for (j = 1; j <=stairs[0].hauteur; j++) {
      int hauteur =stairs[j].hauteur;
      int largeur =stairs[j].largeur;
      int longueur =stairs[j].longueur;
      int x =stairs[j].x;
      int y =stairs[j].y;
      int z =stairs[j].z;
        
      glBegin(GL_QUADS);
      for (i = 0; i < longueur; i++) {
        glColor3f(0.9, 0.9, 0.9);
        glVertex3f(x, y, z);
        glVertex3f(x, y+hauteur, z);
        glVertex3f(x+largeur, y+hauteur, z);
        glVertex3f(x+largeur, y, z);

        glColor3f(0.8, 0.8, 0.8);
        glVertex3f(x, y+hauteur, z);
        glVertex3f(x+largeur, y+hauteur, z);
        glVertex3f(x+largeur, y+hauteur, z+hauteur);
        glVertex3f(x, y+hauteur, z+hauteur);

        y += hauteur;
        z += hauteur;
      } 
      glEnd();
    }
}

/* Fonction d'affichage de tous les sols */
void affiche_all_sol(){
  int i;
  
  for(i = 1; i<=sol[0].hauteur; i++){
    glBegin(GL_QUADS);
    glColor3f(0, 0.7, 0.2);
    glVertex3f(sol[i].pminX, sol[i].pmaxY, sol[i].pminZ);
    glVertex3f(sol[i].pmaxX, sol[i].pmaxY, sol[i].pminZ);
    glVertex3f(sol[i].pmaxX, sol[i].pmaxY, sol[i].pmaxZ);
    glVertex3f(sol[i].pminX, sol[i].pmaxY, sol[i].pmaxZ);
    glEnd();
  }
}

/* Fonction d'affichage de tous les cubes qui composent la map */
void affiche_all_cube() {
  int i;

  for(i = 1; i<=cube[0].hauteur; i++){
    affiche_cube(cube[i].pminX, cube[i].pminY, cube[i].pminZ, cube[i].pmaxX, cube[i].pmaxY, cube[i].pmaxZ, 0.52, 0.37, 0.26);
  }
}

/* Fonction d'affichage de toutes les cloisons */
void affiche_all_cloison(){
  int i;
  for(i = 1; i<=cloisonsMaison[0].hauteur; i++){
    affiche_cube(cloisonsMaison[i].pminX, cloisonsMaison[i].pminY, cloisonsMaison[i].pminZ, cloisonsMaison[i].pmaxX, cloisonsMaison[i].pmaxY, cloisonsMaison[i].pmaxZ, 0.5, 0.5, 0.45);
  }
}

/* Fonction d'affichage d'un arbre donné */
void affiche_arbre (float x, float y, float z, float hauteur_t, float hauteur_a) {
  glBegin(GL_QUADS);
  /* ----------- TRONC ---------- */
  /* face avant tronc (orienté vers z) */
  glColor3f(0.36, 0.25, 0.20);
  glVertex3f(x, y, z);
  glVertex3f(x+1, y, z);
  glVertex3f(x+1, y+hauteur_t, z);
  glVertex3f(x, y+hauteur_t, z);

  /* face droite tronc (orienté vers z) */
  glColor3f(0.36, 0.25, 0.20);
  glVertex3f(x+1, y, z);
  glVertex3f(x+1, y, z+1);
  glVertex3f(x+1, y+hauteur_t, z+1);
  glVertex3f(x+1, y+hauteur_t, z);

  /* face arrière tronc (orienté vers z) */
  glColor3f(0.36, 0.25, 0.20);
  glVertex3f(x, y, z+1);
  glVertex3f(x+1, y, z+1);
  glVertex3f(x+1, y+hauteur_t, z+1);
  glVertex3f(x, y+hauteur_t, z+1);

  /* face gauche tronc (orienté vers z) */
  glColor3f(0.36, 0.25, 0.20);
  glVertex3f(x, y, z);
  glVertex3f(x, y, z+1);
  glVertex3f(x, y+hauteur_t, z+1);
  glVertex3f(x, y+hauteur_t, z);
  /* ----------- TRONC ---------- */

  /* --------- FEUILLES --------- */
  /* face avant feuilles (orienté vers z) */
  glColor3f(0.137255, 0.556863, 0.137255);
  glVertex3f(x-1, y+hauteur_t, z-1);
  glVertex3f(x+2, y+hauteur_t, z-1);
  glVertex3f(x+2, y+hauteur_a, z-1);
  glVertex3f(x-1, y+hauteur_a, z-1);

  /* face droite feuilles (orienté vers z) */
  glColor3f(0.137255, 0.556863, 0.137255);
  glVertex3f(x+2, y+hauteur_t, z-1);
  glVertex3f(x+2, y+hauteur_t, z+2);
  glVertex3f(x+2, y+hauteur_a, z+2);
  glVertex3f(x+2, y+hauteur_a, z-1);

  /* face arrière feuilles (orienté vers z) */
  glColor3f(0.137255, 0.556863, 0.137255);
  glVertex3f(x-1, y+hauteur_t, z+2);
  glVertex3f(x+2, y+hauteur_t, z+2);
  glVertex3f(x+2, y+hauteur_a, z+2);
  glVertex3f(x-1, y+hauteur_a, z+2);

   /* face gauche feuilles (orienté vers z) */
  glColor3f(0.137255, 0.556863, 0.137255);
  glVertex3f(x-1, y+hauteur_t, z-1);
  glVertex3f(x-1, y+hauteur_t, z+2);
  glVertex3f(x-1, y+hauteur_a, z+2);
  glVertex3f(x-1, y+hauteur_a, z-1);

  /* face bas feuilles (orienté vers z) */
  glColor3f(0.137255, 0.556863, 0.137255);
  glVertex3f(x-1, y+hauteur_t, z-1);
  glVertex3f(x+2, y+hauteur_t, z-1);
  glVertex3f(x+2, y+hauteur_t, z+2);
  glVertex3f(x-1, y+hauteur_t, z+2);

  /* face haut feuilles (orienté vers z) */
  glColor3f(0.137255, 0.556863, 0.137255);
  glVertex3f(x-1, y+hauteur_a, z-1);
  glVertex3f(x+2, y+hauteur_a, z-1);
  glVertex3f(x+2, y+hauteur_a, z+2);
  glVertex3f(x-1, y+hauteur_a, z+2);
  /* --------- FEUILLES --------- */

  glEnd();
}

/* Fonction d'affichage de tous les arbres */
void affiche_all_arbre () {
  int i;
  for (i = 1; i <=arbre[0].hauteur; i++) {
    affiche_arbre (arbre[i].pminX, arbre[i].pminY, arbre[i].pminZ, 4.5, arbre[i].hauteur);
  }  
}

void afficher_all_petitcube(){
  int i;

  for(i = 1; i<=petitCube[0].hauteur; i++){
    affiche_cube(petitCube[i].pminX, petitCube[i].pminY, petitCube[i].pminZ, petitCube[i].pmaxX, petitCube[i].pmaxY, petitCube[i].pmaxZ, 0.8, 0.498, 0.196);
  }
}

/* ######################################################### */
/* ########## FONCTIONS POUR AFFICHER LES OBJETS ########### */
/* ##################### -- (FIN) -- ####################### */
/* ######################################################### */

void Affichage () {
  //float rotax = 0+(5*cos(angle));
  //float rotay = 0+(5*sin(angle));

  /* Timer */
  /* clock_t difference = clock() - start;
  actual = difference /(CLOCKS_PER_SEC/2000);
  if (actual > stop) {
    printf("Timer fini, GAME OVER \n");
    exit(0);
  }
  char seconde[15];
  sprintf(seconde, "%d", actual/1000);
  glColor3d(1,1,1); // Texte en blanc
	vBitmapOutput(10,20,seconde,GLUT_BITMAP_HELVETICA_18); */
  //vStrokeOutput(10, 10, seconde,GLUT_STROKE_MONO_ROMAN);
  /* Timer */

  /*  Volume de projection  */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); /*on crée une matrice identique*/
  glFrustum(-0.75, 0.75, -0.75, 0.75, 0.75, 100);
  gluLookAt(posX, posY, posZ, posX+vecx, posY+vecy, posZ+vecz, 0, 1, 0);
  //gluLookAt(posX, 30, posZ, posX+vecx, 30+vecy, posZ+vecz, 0, 1, 0);
  /*Fin Volume de projection */

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  /* Affichage des objets dans la scène*/
  //affiche_cube(0, 0, 0, 5, 5, 5);
  //affiche_escalier(0,0,0,5,2,5);
  //affiche_arbre (55, 10, 50, 4.5, 8);
  //affiche_arbre (45, 10, 50, 4.5, 8);
  affiche_all_arbre ();
  affiche_all_stairs();
  affiche_all_sol();
  affiche_all_cube();
  affiche_all_cloison();
  afficher_all_petitcube();
  //affiche_sol ();
  //affiche_premier_etage (10);

  /* Gestion du saut */
  if (jump == 1) {
    posY+=0.15;
    indice+= 0.15;
    if (indice > limite_saut) {
      jump = 0; 
      indice = 0;
    }
  }
  /* Fin gestion du saut */

  /* Gestion des collisions sol(gravité)/escaliers*/
  if(collision_all_cube (cube, posX, posY, posZ, vecx, vecz)) {
    posY+=0.2;
  }

  if (!collision_tab(sol, posX, posY, posZ, vecx, vecz) && jump!=1) {
    chute = 1;
  }
  else {
    chute = 0;
  }

  if(collision_escalier_all (stairs, posX, posY, posZ, vecx, vecz, tab_info)) {
  //if (collision_escalier(0,0,0,5,2,5,perso_contour(posX, posY, posZ, vecx, vecz))) {
    posY+=0.2;
  }
  if (chute == 1) {
    posY-=0.2;
  }

  int test = collision_tab_renvoie_index(petitCube, posX, posY, posZ, vecx, vecz);
  if(test > 0){
    enlever_tab(petitCube, test);
    nbCubes ++;
    if(nbCubes >= 3){
      printf("GAME OVER BRAVO !\n");
      exit(0);
    }
  }

  //printf("%f %f %f \n", posX, posY, posZ);
  //afficher_tab(cloisons);
  glutSwapBuffers();
}

/* Fonction qui gère les angles lors du mouvement souris */
void Animer() {
  if (sens == 0) {
    angle += .01;
    if (angle > 360) angle = 0;
  }
  else {
    angle -= .01;
    if (angle < 0) angle = 360;
  }
  
  glutPostRedisplay();
}

/* Fonction qui gère les déplacements des touches */
void GererClavier(unsigned char touche, int x, int y)
{  
  switch(touche) {
    case ' ' :
          jump=1;
        break;
    case 'z' :
        if (!collision_cloisons(cloisons, posX+1*vecx, posY, posZ+1*vecz, vecx, vecz)) {
          posZ+=0.1*vecz;
          posX+=0.1*vecx;
        }
        break;
    case 's' : 
        if (!collision_cloisons(cloisons, posX-1*vecx, posY, posZ-1*vecz, vecx, vecz)) {
          posZ-=0.1*vecz;
          posX-=0.1*vecx;
        }
        break;
    case 'q' :
        if (!collision_cloisons(cloisons, posX-1*(-vecz), posY, posZ-1*(vecx), vecx, vecz)) {
          posZ-=0.1*(vecx);
          posX-=0.1*(-vecz);
        }
        break;
    case 'd' :
        if (!collision_cloisons(cloisons, posX-1*(vecz), posY, posZ-1*(-vecx), vecx, vecz)) { 
          posX-=0.1*(vecz);
          posZ-=0.1*(-vecx);
        }
        break;
    case 'e' : 
      sens++;
      sens = sens%2;
      break;
    default :
      break;
  }
}

/* Fonctions pour modifier le vecteur direction avec la souris*/
void look (int x, int y) {
    float angle = 0.5;

    float xoffset = lastMouseX-x;
    float yoffset = lastMouseY-y;
    yoffset *= 0.05;
    

    lastMouseX = x;
    lastMouseY = y;

    float theta = degToRad(angle*xoffset);
    float nx = vecx * cosf(theta) + vecz * sin(theta);
    float nz = -vecx * sin(theta) + vecz * cos(theta);

    vecx = nx;
    vecz = nz;
    vecy += yoffset;
}


int main (int argc, char* argv[]) {
  /* Initialisations des tableaux d'objets */
  init_tab (sol);
  init_tab (cloisons);
  init_tab (arbre);
  init_tab (cube);
  init_escalier (stairs);
  cloisons[0].pmaxX = 1;

  /* Création des différents objets */
  //creer_sol ();
  generer_terrain(200, 200);
  //creer_arbre(65, 10, 50, 4.5, 8);
  //creer_arbre(45, 10, 50, 4.5, 8);
  //creer_facette (50, 10, 45);
  //creer_para (50, 9, 40);
  //creer_escalier(0,0,0,5,2,5,'z');
  //creer_escalier(6,0,0,5,2,5,'z');
  //creer_permier_etage (10);

  
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIDTH, HEIGHT);
  glutInitWindowPosition(50, 50);
  glutCreateWindow("Une maison");
  glEnable(GL_DEPTH_TEST);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  //glutSetCursor(GLUT_CURSOR_NONE); //enlever le curseur
    
  glutIdleFunc(Animer);
  glutDisplayFunc(Affichage);
  glutKeyboardFunc(GererClavier);
  glutPassiveMotionFunc(look);

  glutMainLoop();
  return 0;
}