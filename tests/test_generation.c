#include <GL/gl.h>
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include "../include/generation.h"
#include "../include/objets.h"

void Affichage(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-1, 1, -1, 1, 1, 100);
    gluLookAt(-1, -1, 20, 100, 100, 0, 0, 0, 1);

    glMatrixMode(GL_MODELVIEW0_ARB);


    //affiche_cube(float x1, float y1, float z1, float x2, float y2, float z2)
    double ** hm = create_heightmap(100, 100);
    int i, j;
    for(i=0;i<100;i++){
        for(j=0;j<100;j++){
            affiche_cube(i, j, -2, i+1, j+1, hm[i][j] * 10);
        }
    }

    glutSwapBuffers();
}

int main(int argc, char *argv[]){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    /*On fixe la taille et la position de la fenêtre*/
    glutInitWindowSize(1900, 1000);
    glutInitWindowPosition(0, 0);

    /*Creation de la fenêtre*/
    glutCreateWindow("Exercice 1");
    glEnable(GL_DEPTH_TEST);

    glutDisplayFunc(Affichage);

    glutMainLoop();
    exit(EXIT_SUCCESS);
}