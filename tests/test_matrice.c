#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/matrice.h"

#define degToRad(angleInDegrees) ((angleInDegrees) * M_PI / 180.0)
#define radToDeg(angleInRadians) ((angleInRadians) * 180.0 / M_PI)

int main() {
    point vec;
    vec.x = 1; vec.y = 2; vec.z = 3; vec.d = 1;

    matrice rota = mat_rotaX(degToRad(1));
    matrice rotaY = mat_rotaY(degToRad(1));
    afficher(rotaY);
    matrice result = multiplication_point(rotaY, vec);
    afficher(result);
    printf("test \n");
    float x = result.tab[0][0];
    float y = result.tab[1][0];
    float z = result.tab[2][0];

    matrice rotaT = mat_rotaY(degToRad(5));
    matrice result2 = multiplication_point(rotaT, vec);
    afficher(result2);
    printf("x : %f y : %f z : %f \n", x, y, z);
  
}