#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/intersection.h"


int main () {
    praa p;
    p.pminX = 0;
    p.pminY = 0;
    p.pminZ = 0;
    p.pmaxX = 5;
    p.pmaxY = 5;
    p.pmaxZ = 5;

    praa p2;
    /* p2.pminX = 1;
    p2.pminY = 1;
    p2.pminZ = 2.5;
    p2.pmaxX = 2;
    p2.pmaxY = 2.5;
    p2.pmaxZ = 3.5; */
    p2.pminX = -1;
    p2.pminY = -1;
    p2.pminZ = -1;
    p2.pmaxX = -0.0001;
    p2.pmaxY = -0.0001;
    p2.pmaxZ = -0.0001;

    if (inter_praa_praa(p, p2)) printf("intersection \n");
    else printf("pas d'intersection \n");
    praa p3 = perso_contour(1.5, 2, 3, 0.2, 1);
    printf("p : %f %f %f %f %f %f \n", p3.pminX, p3.pminY, p3.pminZ, p3.pmaxX, p3.pmaxY, p3.pmaxZ);
}