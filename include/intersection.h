#ifndef INTERSECTION_H
#define INTERSECTION_H

typedef struct {
    int hauteur;
    float pminX;
    float pmaxX;
    float pminY;
    float pmaxY;
    float pminZ;
    float pmaxZ;
} praa;

typedef struct {
    char orientation;
    float x;
    float y; 
    float z; 
    int largeur;
    int hauteur; 
    int longueur;
} escalier;

typedef struct {
    float cx;
    float cy;
    float cz;
    float rayon;
} sphere;

#define max(a,b) (a>=b?a:b)
#define min(a,b) (a<=b?a:b)

/* Fonction qui renvoie vraie si intersection entre point et praa */
int inter_point_praa (int x, int y, int z, praa p);

/* Fonction qui renvoie vraie si intersection entre praa et praa */
int inter_praa_praa (praa p1, praa p2);

/* Fonction qui renvoie vraie si intersection entre praa et sphere */
int inter_praa_sphere (sphere s, praa p);

/* Fonction qui renvoie vrai si il existe une collision avec un escalier en particulier */
int collision_escalier (int x, int y, int z, int largeur, int hauteur, int longueur, praa p);

/* Fonction qui renvoie vrai si il existe une collision avec n'importe quel escalier */
int collision_escalier_all (escalier *tableau, float x_perso, float y_perso, float z_perso, float vecX, float vecZ, int tab_info[]);

/* Fonction qui renvoie vrai s'il existe une collision avec n'importe quellle cloison */
int collision_cloisons (praa *tableau, float x, float y, float z, float vecX, float vecZ);

/* Fonction qui renvoie le praa étant le contour d'un personnage */
praa perso_contour (float x, float y, float z, float vecX, float vecZ);

/* Fonction qui renvoie le praa étant le contour d'un arbre */
praa arbre_contour (float x, float y, float z, int hauteur_arbre);

/* Fonction qui renvoie le praa étant le contour d'une maison */
praa maison_contour (float x, float y, float z, int hauteur, int longueur, int largeur);

/* Fonction qui renvoie vrai s'il existe une collision avec un cube en particulier */
int collision_cube (praa cube, float x_perso, float y_perso, float z_perso, float vecX, float vecZ);

/* Fonction qui renvoie vrai s'il existe une collision avec n'importe quel cube */
int collision_all_cube (praa *cube, float x_perso, float y_perso, float z_perso, float vecX, float vecZ);

/* Fonction qui renvoie vrai s'il existe une collision avec un élément d'un tableau */
int collision_tab (praa *tableau, int x, int y, int z, float vecX, float vecZ);

/* Fonction qui renvoie vrai s'il existe une collision avec une sphère en particulier */
int collision_sphere (sphere *tableau, int x, int y, int z, float vecX, float vecZ);

/* Fonction qui permet d'ajouter un praa dans un tableau */
void ajouter_tab (praa * tableau, praa p);

/* Fonction qui permet d'ajouter une sphère dans un tableau */
void ajouter_sphere (sphere * tableau, sphere s);

/* Fonction qui permet d'enlever le dernier élément d'un tableau (diminuer la taille) */
praa enlever_dernier (praa * tableau);

/* Fonction qui permet d'enlever une sphère d'un tableau */
sphere enlever (sphere * tableau, float x, float y, float z, float r);

/* Fonction qui permet d'ajouter un escalier dans un tableau d'escalier */
void ajouter_escalier (escalier * tableau, escalier e);

/* Fonction qui permet d'afficher un tableau de praa */
void afficher_tab(praa * tableau);

/* Fonction qui permet d'afficher un tableau d'escaliers */
void afficher_escalier(escalier * tableau);

/* Fonction qui permet d'initialiser un tableau de praa */
praa* init_tab (praa * tableau);

/* Fonction qui permet d'initialiser un tableau d'escaliers */
escalier* init_escalier (escalier * tableau);

/* Fonction qui permet d'enlever l'élément i d'un tableau de praa */
praa enlever_tab (praa * tableau, int i);

int collision_tab_renvoie_index (praa *tableau, int x, int y, int z, float vecX, float vecZ);

#endif