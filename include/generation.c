#include <math.h>
#include <time.h>
#include <stdlib.h>
#include "generation.h"

double *** generer_grille(int largeur, int hauteur){
    double *** grille = malloc(sizeof(double **) * largeur);
    int i, j;
    double x;
    /*Initialising random number generator*/
    srand((unsigned int) time(NULL));
    for(i=0; i<largeur; i++){
        grille[i] = malloc(sizeof(double *) * hauteur);
        for(j=0; j<hauteur;j++){
            x = ((double)rand()/(double)(RAND_MAX/2)) - 1.0; /*double aléatoire entre -1 et 1*/
            grille[i][j] = malloc(sizeof(double) * 2);
            grille[i][j][0] = x;
            grille[i][j][1] = sqrt(1.0 - pow(x, 2)); /*Pour que la norme du vecteur soit toujours 1*/
        }
    }

    return grille;
}

double vecteur_distance(int ix, int iy, double x, double y, double *** grille){
    /*On calcule le vecteur distance (ix, iy) est le noeud auquel on veut calculer le(x,y)*/
    float dx = x - (float)ix;
    float dy = y - (float)iy;

    /*Puis on retourne le produit scalaire (repère orthonormé)*/
    return (dx * grille[ix][iy][0] + dy * grille[ix][iy][1]);
}

double interpolation(float a, float b, float w){
    return (1.0 - w)*a + w*b;
}

double perlin(float x, float y, double *** grille){
    /*On détermine a quelle case de la grille appartient le point (x, y)*/
    int x0 = floor(x);
    int x1 = x0 + 1;
    int y0 = floor(y);
    int y1 = y0 + 1;

    /*On calcule ensuite les poids qui vont être utilisés pour l'interpolation*/
    float wx = x - (float)x0;
    float wy = y - (float)y0;

    /*Ensuite on fait nos interpolations entre les 4 vecteurs distances calculés*/
    float n0, n1, ix0, ix1;
    /*Premièrement les vercteur haut-gauche et bas-gauche de la case*/
    n0 = vecteur_distance(x0, y0, x, y, grille);
    n1 = vecteur_distance(x1, y0, x, y, grille);
    ix0 = interpolation(n0, n1, wx);
    /*Puis les vecteurs haut-droit et bas-droit de la case*/
    n0 = vecteur_distance(x0, y1, x, y, grille);
    n1 = vecteur_distance(x1, y1, x, y, grille);
    ix1 = interpolation(n0, n1, wx);

    /*Enfin on fait une nouvelle interpolation entre nos deux précédentes interpolation selon le poid wy
    pour avoir notre résultat final*/
    return interpolation(ix0, ix1, wy);
}

double ** create_heightmap(int largeur, int hauteur){
    double *** grille = generer_grille((int)(largeur/10 + 1), (int)(hauteur/10 + 1));
    double ** hm = malloc(hauteur * sizeof(double *));
    int i, j;
    double d1, d2;
    d1 = 0; d2 = 0;

    for(i=0; i<hauteur; i++){
        hm[i] = malloc(largeur * sizeof(double));
        for(j=0; j<largeur; j++){
            hm[i][j] = perlin(d1, d2, grille);
            d2 += 0.1;
        }
        d1 += 0.1;
        d2 = 0;
    }

    return hm;
}