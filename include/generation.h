#ifndef GENERATION_H
#define GENERATION_H

/*https://fr.wikipedia.org/wiki/Bruit_de_Perlin utilisé comme référence*/

/*Cree une matrice a deux dimensions de vecteurs de gradient (Vecteurs unité)
Chaque vecteur est placé sur un noeud de la grille, ainsi grille[0][0] est le noeud
supérieur gauche de la grille et grille[0][1] est le voisin de droite de celui ci etc..*/
double *** generer_grille(int largeur, int hauteur);

/*Calcule le vecteur distance entre un point et le noeud-sommet*/
double vecteur_distance(int ix, int iy, double x, double y, double *** grille);

/*Calcule l'interpolation linéaire entre a et b, le poid w appartient a [0, 1]*/
double interpolation(float a, float b, float w);

/*Calcule le bruit de perlin aux coordonées (x, y)*/
double perlin(float x, float y, double *** grille);

/*Crée la heightmap en utilisant le bruit de perlin*/
double ** create_heightmap(int largeur, int hauteur);

#endif