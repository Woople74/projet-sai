#include <stdio.h>
#include <stdlib.h>
#include "intersection.h"
#include "math.h"

/* Fonction qui renvoie vraie si intersection entre point et praa */
int inter_point_praa (int x, int y, int z, praa p) {
    if ((p.pminX <= x && p.pmaxX >= x) && (p.pminY <= y && p.pmaxY >= y) && (p.pminZ <= z && p.pmaxZ >= z)) return 1;
    else return 0;
}

/* Fonction qui renvoie vraie si intersection entre praa et praa */
int inter_praa_praa (praa p1, praa p2) {
    if ((p1.pminX <= p2.pmaxX) && (p1.pmaxX >= p2.pminX) &&
    (p1.pminY <= p2.pmaxY) && (p1.pmaxY >= p2.pminY) &&
    (p1.pminZ <= p2.pmaxZ) && (p1.pmaxZ >= p2.pminZ)) return 1;
    else return 0;
}

/* Fonction qui renvoie vraie si intersection entre praa et sphere */
int inter_praa_sphere (sphere s, praa p) {
    float hx = max(p.pminX, min(s.cx, p.pmaxX));
    float hy = max(p.pminY, min(s.cy, p.pmaxY));
    float hz = max(p.pminZ, min(s.cz, p.pmaxZ));

    if ((powf(hx-s.cx, 2) + pow(hy-s.cy, 2) + pow(hz-s.cz, 2)) <= pow(s.rayon, 2)) return 1;
    else return 0;
}

/* Fonction qui renvoie le praa étant le contour d'un personnage */
praa perso_contour (float x, float y, float z, float vecX, float vecZ) {
    praa p;
    /* p.pminX = x-0.5;
    p.pminY = y-2;
    p.pminZ = z-0.5*(vecX);
    p.pmaxX = x+0.5*(vecX);
    p.pmaxY = y+0.5;
    p.pmaxZ = z+0.5; */

    p.pminX = x-0.5;
    p.pminY = y-2;
    p.pminZ = z-0.5;
    p.pmaxX = x+0.5;
    p.pmaxY = y+0.5;
    p.pmaxZ = z+0.5;

    return p;
}

/* Fonction qui renvoie le praa étant le contour d'un arbre */
praa arbre_contour (float x, float y, float z, int hauteur_arbre) {
    praa p;
    p.hauteur = hauteur_arbre;
    p.pminX = x-1;
    p.pminY = y;
    p.pminZ = z-1;
    p.pmaxX = x+2;
    p.pmaxY = y+hauteur_arbre;
    p.pmaxZ = z+2;

    return p;
}

/* Fonction qui renvoie le praa étant le contour d'une maison */
praa maison_contour (float x, float y, float z, int hauteur, int longueur, int largeur) {
    praa p;
    p.hauteur = hauteur;
    p.pminX = x;
    p.pminY = y;
    p.pminZ = z;
    p.pmaxX = x+largeur;
    p.pmaxY = y+hauteur;
    p.pmaxZ = z+longueur;

    return p;
}

/* Fonction qui renvoie vrai si il existe une collision avec un escalier en particulier */
int collision_escalier (int x, int y, int z, int largeur, int hauteur, int longueur, praa p) {
    int i;

    for (i = 0; i < longueur; i++) {
        praa escalier;
        escalier.hauteur = hauteur;
        escalier.pmaxX = x+largeur;
        escalier.pmaxY = y+hauteur;
        escalier.pmaxZ = z+hauteur;
        escalier.pminX = x;
        escalier.pminY = y+hauteur;
        escalier.pminZ = z;

        if (inter_praa_praa(escalier, p)) return 1;

        y += hauteur;
        z += hauteur;
    }

    return 0;
}

/* Fonction qui renvoie vrai si il existe une collision avec n'importe quel escalier */
int collision_escalier_all (escalier *tableau, float x_perso, float y_perso, float z_perso, float vecX, float vecZ, int tab_info[]) {
    int i, j;
    for (j = 1; j <= tableau[0].hauteur; j++) {
        int hauteur = tableau[j].hauteur;
        int largeur = tableau[j].largeur;
        int longueur = tableau[j].longueur;
        int x = tableau[j].x;
        int y = tableau[j].y;
        int z = tableau[j].z;
        
        for (i = 0; i < longueur; i++) {
            /* praa escalier;
            escalier.hauteur = hauteur;
            escalier.pmaxX = x+largeur;
            escalier.pmaxY = y+hauteur;
            escalier.pmaxZ = z;
            escalier.pminX = x;
            escalier.pminY = y;
            escalier.pminZ = z; */

            praa escalier;
            escalier.hauteur = hauteur;
            escalier.pmaxX = x+largeur;
            escalier.pmaxY = y+hauteur;
            escalier.pmaxZ = z+hauteur;
            escalier.pminX = x;
            escalier.pminY = y+hauteur;
            escalier.pminZ = z;

            praa p = perso_contour(x_perso, y_perso, z_perso, vecX, vecZ);
            if (inter_praa_praa(escalier, p)) {
                //if (peut_monter (escalier, p.pminX, p.pminY, p.pminZ)) return 1;
                //else return 0;
                tab_info[0] = j;
                tab_info[1] = i+1;
                tab_info[2] = 1;
                return 1;
            }

            y += hauteur;
            z += hauteur;
        }
    }
    return 0;
} 

/* int collision_sol (praa *tableau, int x, int y, int z, float vecX, float vecZ) {
    int taille = tableau[0].hauteur;
    int i;

    for (i = 1; i <= taille; i++) {
        praa p;
        p.hauteur = tableau[i].hauteur;
        p.pmaxX = tableau[i].pmaxX;
        p.pmaxY = tableau[i].pmaxY;
        p.pmaxZ = tableau[i].pmaxZ;
        p.pminX = tableau[i].pminX;
        p.pminY = tableau[i].pminY;
        p.pminZ = tableau[i].pminZ;

        if (inter_praa_praa(p, perso_contour(x, y, z, vecX, vecZ))) {
            return 1;
        }
    }
    return 0;
} */

/* Fonction qui renvoie vrai s'il existe une collision avec un cube en particulier */
int collision_cube (praa cube, float x_perso, float y_perso, float z_perso, float vecX, float vecZ) {
    int hauteur = cube.hauteur;

    praa p = perso_contour(x_perso, y_perso, z_perso, vecX, vecZ);
    if (inter_praa_praa(cube, p) && hauteur == 1) return 1;
    return 0;
}

/* Fonction qui renvoie vrai s'il existe une collision avec n'importe quel cube */
int collision_all_cube (praa *cube, float x_perso, float y_perso, float z_perso, float vecX, float vecZ) {
    int i;
    int taille = cube[0].hauteur;

    for (i = 0; i < taille; i++) {
        if (inter_praa_praa(cube[i], perso_contour(x_perso, y_perso, z_perso, vecX, vecZ))) {
            return 1;
        }
    }
    return 0;
}

/* Fonction qui renvoie vrai s'il existe une collision avec un élément d'un tableau */
int collision_tab (praa *tableau, int x, int y, int z, float vecX, float vecZ) {
    int taille = tableau[0].hauteur;
    int i;

    for (i = 1; i <= taille; i++) {
        /* praa p;
        p.hauteur = tableau[i].hauteur;
        p.pmaxX = tableau[i].pmaxX;
        p.pmaxY = tableau[i].pmaxY;
        p.pmaxZ = tableau[i].pmaxZ;
        p.pminX = tableau[i].pminX;
        p.pminY = tableau[i].pminY;
        p.pminZ = tableau[i].pminZ; */

        if (inter_praa_praa(tableau[i], perso_contour(x, y, z, vecX, vecZ))) {
            return 1;
        }
    }
    return 0;
}

/* Fonction qui renvoie vrai s'il existe une collision avec une sphère en particulier */
int collision_sphere (sphere *tableau, int x, int y, int z, float vecX, float vecZ) {
    int taille = tableau[0].rayon;
    int i;

    for (i = 1; i <= taille; i++) {
        if (inter_praa_sphere(tableau[i], perso_contour(x, y, z, vecX, vecZ))) {
            return 1;
        }
    }
    return 0;
}

/* Fonction qui renvoie vrai s'il existe une collision avec n'importe quellle cloison */
int collision_cloisons (praa *tableau, float x, float y, float z, float vecX, float vecZ) {
    int taille = tableau[0].hauteur;
    int i;

    for (i = 1; i <= taille; i++) {
        if (inter_praa_praa(tableau[i], perso_contour(x, y, z, vecX, vecZ))) {
            return 1;
        }
    }
    return 0;
}

/* Fonction qui permet d'ajouter un praa dans un tableau */
void ajouter_tab (praa * tableau, praa p) {
    int taille = tableau[0].hauteur;
    tableau[taille+1] = p;
    tableau[0].hauteur++;
}

/* Fonction qui permet d'ajouter une sphère dans un tableau */
void ajouter_sphere (sphere * tableau, sphere s) {
    int taille = tableau[0].rayon;
    tableau[taille+1] = s;
    tableau[0].rayon++;
}

/* Fonction qui permet d'enlever le dernier élément d'un tableau (diminuer la taille) */
praa enlever_dernier (praa * tableau) {
    int taille = tableau[0].hauteur;
    praa p = tableau[taille];
    tableau[0].hauteur--;
    return p;
}

/* Fonction qui permet d'enlever une sphère d'un tableau */
sphere enlever (sphere * tableau, float x, float y, float z, float r) {
    int taille = tableau[0].rayon;
    int i;
    sphere s;

    for (i = 1; i <= taille; i++) {
        if ((tableau[i].cx == x) && (tableau[i].cy == y) && (tableau[i].cz == z) && (tableau[i].rayon == r)) {
            s = tableau[i];
            if (i != taille) {
                tableau[i].cx = tableau[i+1].cx;
                tableau[i].cy = tableau[i+1].cy;
                tableau[i].cz = tableau[i+1].cz;
                tableau[i].rayon = tableau[i+1].rayon;
            }
        }
    }
    tableau[0].rayon--;
    return s;
}

/* Fonction qui permet d'ajouter un escalier dans un tableau d'escalier */
void ajouter_escalier (escalier * tableau, escalier e) {
    int taille = tableau[0].hauteur;
    tableau[taille+1] = e;
    tableau[0].hauteur++;
}

/* Fonction qui permet d'afficher un tableau de praa */
void afficher_tab(praa * tableau) {
    int i;
    for (i = 1; i <= tableau[0].hauteur; i++) {
        printf(" (%d | %f | %f | %f | %f | %f | %f )\n", tableau[i].hauteur, tableau[i].pmaxX, tableau[i].pmaxY, tableau[i].pmaxZ, tableau[i].pminX, tableau[i].pminY, tableau[i].pminZ);
    }
}

/* Fonction qui permet d'afficher un tableau d'escaliers */
void afficher_escalier(escalier * tableau) {
    int i;
    for (i = 1; i <= tableau[0].hauteur; i++) {
        printf(" (%d | %d | %d | %c | %f | %f | %f )\n", tableau[i].hauteur, tableau[i].largeur, tableau[i].longueur, tableau[i].orientation, tableau[i].x, tableau[i].y, tableau[i].z);
    }
}

/* Fonction qui permet d'initialiser un tableau de praa */
praa* init_tab (praa * tableau) {
    int i;
    for (i = 0; i < 1000; i++) {
        tableau[i].hauteur = 0;
        tableau[i].pmaxX = 0;
        tableau[i].pmaxY = 0;
        tableau[i].pmaxZ = 0;
        tableau[i].pminX = 0;
        tableau[i].pminY = 0;
        tableau[i].pminZ = 0;
    }

    return tableau;
}

/* Fonction qui permet d'initialiser un tableau d'escaliers */
escalier* init_escalier (escalier * tableau) {
    int i;
    for (i = 0; i < 1000; i++) {
        tableau[i].hauteur = 0;
        tableau[i].largeur = 0;
        tableau[i].longueur = 0;
        tableau[i].x = 0;
        tableau[i].y = 0;
        tableau[i].z = 0;
        tableau[i].orientation = 0;
    }
    return tableau;
}

/* Fonction qui permet d'enlever l'élément i d'un tableau de praa */
praa enlever_tab (praa * tableau, int i) {
    int taille = tableau[0].hauteur;
    int j;
    praa p = tableau[i];

    //On décalle tout le tableau a gauche a partie de la ou on supprime
    for (j = i; j < taille; j++) {
        tableau[i].hauteur = tableau[i+1].hauteur;
        tableau[i].pmaxX = tableau[i+1].pmaxX;
        tableau[i].pmaxY = tableau[i+1].pmaxY;
        tableau[i].pmaxZ = tableau[i+1].pmaxZ;
        tableau[i].pminX = tableau[i+1].pminX;
        tableau[i].pminY = tableau[i+1].pminY;
        tableau[i].pminZ = tableau[i+1].pminZ;
    }
    //Puis on réinitialise le dernier élément du tableau
    tableau[taille].hauteur = 0;
    tableau[taille].pmaxX = 0;
    tableau[taille].pmaxY = 0;
    tableau[taille].pmaxZ = 0;
    tableau[taille].pminX = 0;
    tableau[taille].pminY = 0;
    tableau[taille].pminZ = 0;

    tableau[0].hauteur -= 1;
    return p;
}


/* Identique a collision_tab mais renvoie l'index dans le tableau */
int collision_tab_renvoie_index (praa *tableau, int x, int y, int z, float vecX, float vecZ) {
    int taille = tableau[0].hauteur;
    int i;

    for (i = 1; i <= taille; i++) {
        if (inter_praa_praa(tableau[i], perso_contour(x, y, z, vecX, vecZ))) {
            return i;
        }
    }
    return 0;
}