#ifndef COULEURS_H
#define COULEURS_H

/*On défini quelques couleurs pour rendre leur utilisation plus facile*/
#define ROUGE glColor3f(0.6, 0, 0.1);
#define VERT glColor3f(0, 0.6, 0.1);
#define BLEU glColor3f(0, 0.1, 0.6);
#define NOIR glColor3f(0, 0, 0);
#define BLANC glColor3f(1, 1, 1);
#define ORANGE glColor3f(1, 0.8, 0);
#define VIOLET glColor3f(0.9, 0, 0.8);
#define JAUNE glColor3f(1, 0.7, 0.1);
#define GRIS glColor3f(0.5, 0.5, 0.5);

#endif