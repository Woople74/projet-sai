/*
 * Ce fichier contient les fonctions permettant d'afficher des objets prédéfinis a une position donnée en paramètre
 * On peut également appliquer une rotation a ces objets
 */

#ifndef OBJET_H
#define OBJET_H

void affiche_cube(double x1, double y1, double z1, double x2, double y2, double z2, double r, double g, double b);

#endif