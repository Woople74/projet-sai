#ifndef MATRICE_H
#define MATRICE_H

typedef struct {
    int x;
    int y;
    int z;
    int d;
} point;

typedef struct {
    int ligne;
    int colonne;
    float **tab;
} matrice;


matrice init (int l, int c);

void afficher (matrice m);

matrice zero(matrice m);

matrice multiplication_matrice(matrice m1, matrice m2);

matrice multiplication_point(matrice m, point p);

matrice mat_rotaX(float teta);

matrice mat_rotaY (float teta);

#endif