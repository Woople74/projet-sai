#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "matrice.h"

matrice init (int l, int c) {
    int i, j;
    matrice m;
    m.ligne = l;
    m.colonne = c;
    m.tab = (float**) malloc(l * sizeof(float*));
    for (i = 0; i < l; i++) {
        m.tab[i] = (float*)malloc(c * sizeof(float));
    }
    for (i = 0; i < m.ligne; i++) {
	    for (j = 0; j < m.colonne; j++){
	        m.tab[i][j] = 0;
	    }
    }
    return m;
}

void afficher (matrice m) {
  printf("\n");
  int i, j;
  for (i = 0; i < m.ligne; i++) {
        for (j = 0; j < m.colonne; j++) {
            printf("%10f |", m.tab[i][j]);
        }
    printf("\n");
    }
}

matrice zero(matrice m) {
  int i, j;
  for (i = 0; i < m.ligne; i++) {
        for (j = 0; j < m.colonne; j++) {
        m.tab[i][j] = 0;
        }
    }
  return m;
}

matrice multiplication_matrice(matrice m1, matrice m2) {
    if (m1.colonne == m2.ligne) {
      int i, j, k;
      matrice m = init(m1.colonne, m2.ligne);
      for (i = 0; i < m.ligne; i++) {
	    for (j = 0; j < m.colonne; j++){
	        for (k = 0; k < m.colonne; k++) {
		        m.tab[i][j] += m1.tab[i][k]*m2.tab[k][j];
		    }
	    }
	}
      return m;
    }
  else {
      printf("Erreur les matrices ne peuvent pas être multipliées : mat1(m1,n1) et mat2(m2,n2) avec n1 = m2 \n");
      exit(-1);
    }
}

matrice multiplication_point(matrice m, point p) {
    int i, j;
    matrice p1 = init(4, 1);
    matrice result = init(4, 1);
    p1.tab[0][0] = p.x;
    p1.tab[1][0] = p.y;
    p1.tab[2][0] = p.z;
    p1.tab[3][0] = p.d;

    //afficher(p1);

    for (i = 0; i < m.ligne; i++) {
        result.tab[i][0] = 0;
        for (j = 0; j < m.colonne; j++) {
            result.tab[i][0] += m.tab[i][j]*p1.tab[j][0];
            printf("%f %f %f \n", result.tab[i][0], m.tab[i][j], p1.tab[j][0]);
        }
    }

    return result;
}

matrice mat_rotaX (float teta) {
    matrice T0= init(4, 4);
    T0.tab[0][0] = 1; T0.tab[0][1] = 0;         T0.tab[0][2] = 0;           T0.tab[0][3] = 0;
    T0.tab[1][0] = 0; T0.tab[1][1] = cosf(teta); T0.tab[1][2] = -sinf(teta);  T0.tab[1][3] = 0;
    T0.tab[2][0] = 0; T0.tab[2][1] = sinf(teta); T0.tab[2][2] = cosf(teta);   T0.tab[2][3] = 0;
    T0.tab[3][0] = 0; T0.tab[3][1] = 0;         T0.tab[3][2] = 0;           T0.tab[3][3] = 1;
    return T0;
}

matrice mat_rotaY (float teta) {
    matrice T0= init(4, 4);
    T0.tab[0][0] = cosf(teta);  T0.tab[0][1] = 0; T0.tab[0][2] = sinf(teta);    T0.tab[0][3] = 0;
    T0.tab[1][0] = 0;           T0.tab[1][1] = 1; T0.tab[1][2] = 0;             T0.tab[1][3] = 0;
    T0.tab[2][0] = -sinf(teta); T0.tab[2][1] = 0; T0.tab[2][2] = cosf(teta);    T0.tab[2][3] = 0;
    T0.tab[3][0] = 0;           T0.tab[3][1] = 0; T0.tab[3][2] = 0;             T0.tab[3][3] = 1;
    return T0;
}
