CC = gcc -Wall -g 
GLIBS = -lGL -lGLU -lglut

all : clean main

tests : clean test_generation test_matrice test_intersec

main : main.c bin/matrice.o bin/intersection.o bin/generation.o bin/objets.o
	$(CC) main.c bin/matrice.o bin/intersection.o bin/generation.o bin/objets.o -o main -lm $(GLIBS)

test_matrice : tests/test_matrice.c bin/matrice.o
	$(CC) tests/test_matrice.c bin/matrice.o -o test_matrice -lm

test_intersec : tests/test_collision.c bin/intersection.o
	$(CC) tests/test_collision.c bin/intersection.o -o test_intersection -lm

test_generation : tests/test_generation.c bin/generation.o bin/objets.o
	$(CC) tests/test_generation.c bin/generation.o bin/objets.o -o test_generation -lm $(GLIBS)

bin/objets.o : include/objets.c
	$(CC) -c include/objets.c -o bin/objets.o $(GLIBS)

bin/generation.o : include/generation.c
	$(CC) -c include/generation.c -o bin/generation.o -lm

bin/intersection.o : include/intersection.c
	$(CC) -c include/intersection.c -o bin/intersection.o -lm

bin/matrice.o : include/matrice.c 
	$(CC) -c include/matrice.c  -o bin/matrice.o -lm

clean :
	rm -f main test_* bin/*.o